function ggg (func1,func2){
    return func1() + func2();
  }
  
  let result = ggg(
    function(){
      return 3;
    },
    function (){
      return 4;
    }
  )
  
  document.write(result);